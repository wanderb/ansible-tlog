# Configure tlog/sssd-session-recording with Ansible

This role configures session recording on RHEL8 with tlog and sssd

## Variables

This roles uses a couple of variables:
- `ansible_tlog_users`
  A list of users for who sessions should be recorded

  Default: []
- `ansible_tlog_groups`
  A list of groups for who sessions should be recorded

  Default: []

- `ansible_tlog_enable_cockpit`
  Whether or not the Cockpit module should be installed and configured

  Default: true

If neither `ansible_tlog_users` nor `ansible_tlog_groups` is configured **all**
sessions will be recorded.


## Example playbook
```yaml
---
- name: Install and configure terminal logging
  hosts:
  - all
  become: yes
  gather_facts: yes
  roles:
  - role: ansible-tlog
    ansible_tlog_groups:
    - wheel
    ansible_tlog_users:
    - alice
    - bob
    - mary
```
